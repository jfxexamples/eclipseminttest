package application;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class SampleController {
	
	@FXML
	private TabPane tabPane;
	
	public void createTab() {
		
		Tab tab = new Tab("New tab");//Breakpoint here does NOT freeze desktop
		
//		tab.setOnCloseRequest(e -> {
//			System.out.println("bleh");//Breakpoint here, freezes desktop
//		});
		tab.setOnCloseRequest(new EventHandler<Event>(){
		    @Override public void handle(Event e){
		    	System.out.println("bleh");//Breakpoint here, also freezes desktop
		    }
		});
		
		tabPane.getTabs().add(tab);//Breakpoint here does NOT freeze desktop
		int index = tabPane.getTabs().size() - 1;
		tabPane.getSelectionModel().select(index);
	}
}
